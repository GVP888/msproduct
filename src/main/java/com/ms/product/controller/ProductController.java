package com.ms.product.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product")
public class ProductController {
	
	
	private static final Logger log = LoggerFactory.getLogger(ProductController.class);

	@Value("${server.port}")
	private String portNo;
	
	@Value("${spring.application.name}")
	private String msName;
	
	@GetMapping("/show")
	public String show() {
		log.info("Entered ProductController :: show()");
		System.out.println("Entered ProductController :: show()");
		String msg = "Successfully run the"+msName+"-"+portNo;
		return msg;
	}

}
