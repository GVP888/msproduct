package com.ms.product;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class MsProductApplication {
    
	private static final Logger log = LoggerFactory.getLogger(MsProductApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(MsProductApplication.class, args);
		log.info("Started Prodcut Micro Service");
		System.out.println("MsProductApplication :: main() ::: Started Prodcut MS - 3101");
		
		System.out.println("### Code is commiting in Bitbucket");
	}

}
